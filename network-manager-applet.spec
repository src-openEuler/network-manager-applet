%global gtk3_version    %(pkg-config --modversion gtk+-3.0 2>/dev/null || echo bad)
%global glib2_version   %(pkg-config --modversion glib-2.0 2>/dev/null || echo bad)
%global nm_version      1:1.16.0
%global libnma_version  1.8.27
%global obsoletes_ver   1:0.9.7
%global real_version 1.36.0
%global real_version_major %(printf '%s' '%{real_version}' | sed -n 's/^\\([1-9][0-9]*\\.[1-9][0-9]*\\)\\.[1-9][0-9]*$/\\1/p')
 
Name: network-manager-applet
Summary: A network control and status applet for NetworkManager
Version: 1.36.0
Release: 1
License: GPLv2+
URL: http://www.gnome.org/projects/NetworkManager/
Obsoletes: NetworkManager-gnome < %{obsoletes_ver}
Source:  https://download.gnome.org/sources/network-manager-applet/1.36/%{name}-%{version}.tar.xz

Requires: NetworkManager >= %{nm_version} libnotify >= 0.4.3 nm-connection-editor libnma

Patch1:   Modify-the-prompt-for-cloning-a-MAC-address.patch

BuildRequires: NetworkManager-libnm-devel >= %{nm_version} libnma-devel ModemManager-glib-devel >= 1.0 glib2-devel >= 2.32 gtk3-devel >= 3.10
BuildRequires: gobject-introspection-devel >= 0.10.3 gettext-devel pkgconfig libnotify-devel >= 0.4 meson intltool gtk-doc
BuildRequires: /usr/bin/autopoint mobile-broadband-provider-info-devel libselinux-devel  libappindicator-gtk3-devel libdbusmenu-gtk3-devel
BuildRequires: desktop-file-utils iso-codes-devel libsecret-devel >= 0.12 jansson-devel gcr-devel

%description
This package contains a network control and status notification area applet
for use with NetworkManager.
 
%package -n nm-connection-editor
Summary: A network connection configuration editor for NetworkManager
Requires: libnma
 
%description -n nm-connection-editor
This package contains a network configuration editor and Bluetooth modem
utility for use with NetworkManager.

%package_help

%package -n nm-connection-editor-desktop
Summary: The desktop file for nm-connection-editor
Requires: nm-connection-editor%{?_isa} = %{version}-%{release}
 
%description -n nm-connection-editor-desktop
This package contains the desktop file and appdata for nm-connection-editor.
Without it, the nm-connection-editor cannot be started from the desktop
environment.

%prep
%autosetup -n %{name}-%{real_version} -p1
cd po/
rm -f zh_CN.gmo && msgfmt zh_CN.po -o zh_CN.gmo
cd -
 
%build
%meson \
    -Dselinux=true \
    -Dappindicator=yes
%meson_build
%install
%meson_install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/gnome-vpn-properties
 
%find_lang nm-applet
cat nm-applet.lang >> %{name}.lang
 
desktop-file-validate $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/nm-applet.desktop
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/nm-connection-editor.desktop
 
%check
%meson_test
 
%files
%defattr(-,root,root)
%doc NEWS CONTRIBUTING
%license COPYING
%{_bindir}/nm-applet
%{_datadir}/applications/nm-applet.desktop
%{_datadir}/icons/hicolor/22x22/apps/nm-adhoc.png
%{_datadir}/icons/hicolor/22x22/apps/nm-insecure-warn.png
%{_datadir}/icons/hicolor/22x22/apps/nm-mb-roam.png
%{_datadir}/icons/hicolor/22x22/apps/nm-secure-lock.png
%{_datadir}/icons/hicolor/22x22/apps/nm-signal-*.png
%{_datadir}/icons/hicolor/22x22/apps/nm-stage*-connecting*.png
%{_datadir}/icons/hicolor/22x22/apps/nm-tech-*.png
%{_datadir}/icons/hicolor/22x22/apps/nm-vpn-active-lock.png
%{_datadir}/icons/hicolor/22x22/apps/nm-vpn-connecting*.png
%{_datadir}/icons/hicolor/22x22/apps/nm-wwan-tower.png
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%exclude %{_datadir}/glib-2.0/schemas/org.gnome.nm-applet.gschema.xml
%{_datadir}/GConf/gsettings/nm-applet.convert
%{_sysconfdir}/xdg/autostart/nm-applet.desktop

%files -n nm-connection-editor -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/nm-connection-editor
%{_datadir}/icons/hicolor/*/apps/nm-device-*.*
%{_datadir}/icons/hicolor/*/apps/nm-no-connection.*
%{_datadir}/icons/hicolor/16x16/apps/nm-vpn-standalone-lock.png
%dir %{_datadir}/gnome-vpn-properties

%files      help
%defattr(-,root,root)
%{_mandir}/man1/nm-applet*
%{_mandir}/man1/nm-connection-editor*

%files -n nm-connection-editor-desktop
%{_datadir}/applications/nm-connection-editor.desktop
%{_datadir}/metainfo/nm-connection-editor.appdata.xml

%changelog
* Tue Jan 16 2024 zhangpan <zhangpan103@h-partners.com> - 1.36.0-1
- update to 1.36.0

* Thu Jul 20 2023 zhangpan <zhangpan103@h-partners.com> - 1.32.0-1
- update to 1.32.0

* Tue Feb 14 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.30.0-2
- fix file conflicts and remove old tar

* Tue Feb 07 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.30.0-1
- update to 1.30.0

* Mon Jan 09 2023 zhangpan<zhangpan@h-partners.com> - 1.24.0-4
- Type:bugfix
- CVE:
- DESC:Modify the prompt for cloning a MAC address

* Mon Jun 20 2022 wangkerong <wangkerong@h-partners.com> - 1.24.0-3
- delete unnecessary meson options and fix build faile on meson-0.6

* Mon Feb 28 2022 xingxing <xingxing9@h-partners.com> - 1.24.0-2
- fix duplicate packaging 

* Mon Dec 06 2021 xingxing <xingxing9@huawei.com> - 1.24.0-1
- Update to 1.24.0 

* Thu Sep 10 2020 chengguipeng<chengguipeng1@huawei.com> - 1.18.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source0 url

* Fri Jul  31 2020 wuchaochao <wuchaochao4@huawei.com> - 1.18.0-1
- Update to 1.18.0 release 
